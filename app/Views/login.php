
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="<?php echo base_url();?>/dist/css/bootstrap.css">
		
		<link rel="stylesheet" href="<?php echo base_url();?>/resources/css/estilo.css">
<main>



            <div class="contenedor__todo">
                
                <div class="caja__trasera">
                    <div class="caja__trasera-login">
                        <h3>¿Ya tienes una cuenta?</h3>
                        <p>Inicia sesión para entrar en la página</p>
                        <button id="btn__iniciar-sesion">Iniciar Sesión</button>
                    </div>
                    <div class="caja__trasera-register">
                        <h3>¿Aún no tienes una cuenta?</h3>
                        <p>Regístrate para que puedas iniciar sesión</p>
                        <button id="btn__registrarse">Regístrarse</button>
                    </div>
                </div>

                <!--Formulario de Login y registro-->
                <div class="contenedor__login-register">
                 <!--PHP-->
                <form class="formulario__login" action="<?php echo base_url();?>/login/doLogin">
				 <?php $validation = \Config\Services::validation(); ?>

				<!-- Error -->
                            <?php if($validation->getError('user')) {?>
                                <div class='alert alert-danger mt-2'>
                                  <?= $error = $validation->getError('user'); ?>
                                </div>
                            <?php }?>
                            <!-- Error -->
                            <?php if($validation->getError('pass')) {?>
                                <div class='alert alert-danger mt-2'>
                                  <?= $error = $validation->getError('pass'); ?>
                                </div>
                            <?php }?>
                    <!--Login-->
                   
                    
                    
                        
                        <h2>Iniciar Sesión</h2>
                        <div class="textbox">
                        <input type="email" class="form-control" id="user"
                     name="user" title="Formato correo" placeholder="Ingresar Correo" required
                        >
                        
                      <!--  <input type="text" placeholder="Correo Electronico">-->
                      <input type="password" id="pass" name="pass" class="form-control" placeholder="Password" required>
                      </div>
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
                    </form>
                        

                    

                    <!--Register-->
                    <form action="" class="formulario__register">
                        <h2>Regístrarse</h2>
                        <input type="text" placeholder="Nombre completo">
                        <input type="text" placeholder="Correo Electronico">
                        <input type="text" placeholder="Usuario">
                        <input type="password" placeholder="Contraseña">
                        <button>Regístrarse</button>
                    </form>
                </div>
            </div>

        </main>
       
        <div class="container marketing">

        <div class="hint">Volver</div>
        <div class="socials-container">
          <!-- Volver -->
          <a class="social-link" href="<?php echo base_url();?>/Home">
            <div class="social-wrapper codepen">
            <button type="button" class="btn btn-dark">VOLVER</button>
            </div>
          </a>
<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../../../assets/js/ie10-viewport-bug-workaround.js"></script>
<!-- Three columns of text below the carousel -->
<br>
<div class="row">
    <div class="col-sm-12">
<hr class="featurette-divider">
<script src="<?= base_url()?>/resources/js/login.js"></script>


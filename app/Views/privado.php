<script src="<?= base_url()?>/resources/js/jsPrivado.js"></script>
<script>
  ruta='<?= base_url()?>';  
  $(document).ready(function() {
    doaction();
  });
</script>




<div class="contenedor1">
<figure>
	<img src="<?php echo base_url();?>/resources/img/portada1.jpg" alt="">
	<div class="cap"><h3>REGISTRO DE DENUNCIA</h3>
		<p>LOS VALORES QUE INGRESE DEBEN SER 100% REALES.</p>
		
	
	</div>
    </div>
    </figure>



	<section>
  <div class="container">
    <div class="row">
    <div class="col-md-4"><!--Inzq  -->

            <div class="card">
               <div class="card-header">
                    <h3 class="card-title"></h3>
                </div>
                <br>
                <img class="card-img-top" src="<?php echo base_url();?>/resources/img/registro.png" width="20" height="150" alt="Card image cap">
                <div class="card-body">

                  <div class="alert alert-danger"  id="error" >
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
              <p id="mensaje_error"></p>
            </div>        
            <div class="alert alert-success"  id="succ" >
                <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
                    <span class="sr-only">Correcto:</span>
              <p id="mensaje_ok"></p>
            </div>        
                   <?php $validation = \Config\Services::validation(); ?>

                   <?= form_open_multipart('#', array('id' => 'frmreg','name' => 'frmreg')) ?>
                          

                      <div class="input-group input-group-lg">
                       <span class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                       
                       </span>
                        <input type="text" class="form-control" id="docid" name="docid" required title="Solo digitos numericos" placeholder="Ingresar DNI">
                          <!-- Error -->
                            <?php if($validation->getError('docid')) {?>
                                <div class='alert alert-danger mt-2'>
                                  <?= $error = $validation->getError('docid'); ?>
                                </div>
                            <?php }?>
                        
                      </div>    
                      <!-- FORMULARIO PRIVADO -->   
                             
                      <div class="form-group">
                        <label for="nombre">* Nombre:</label>
                        <input type="text" class="form-control" id="nombre"
                         name="nombre" title="Solo alfanumericos" placeholder="Ingresar Nombre" required>
                      </div>
                      <div class="form-group">
                        <label for="apel">* Apellidos:</label>
                        <input type="text" class="form-control" id="apel"
                     name="apel" title="Solo alfanumericos" placeholder="Ingresar Apellidos" required
                        >
                      </div>
                      <div class="form-group">   

                        <label for="cel">Celular:</label>
                        <input type="cel" class="form-control" id="cel"
                     name="cel" title="Solo digitos" placeholder="Ingresar Telefono" 
                        >

                      </div>
                    <div class="form-group">
                        <label for="correo">* Correo:</label>
                        <input type="email" class="form-control" id="correo"
                     name="correo" title="Formato correo" placeholder="Ingresar Correo" required
                        >
                      </div>
                    <div class="form-group">
                        <label for="dir">* Direccion y Referencia:</label>
                        <input type="text" class="form-control" id="dir"
                     name="dir" title="Solo alfanumericos" placeholder="Ingresar Direccion" required
                        >
                      </div>
                      <div class="form-group">
                        <label for="det">Detalle de lo sucedido:</label>
                        <small>(Obligatorio)</small>
                        <textarea class="form-control" id="det" name="det"
                        rows="3" required></textarea>
                        </div>
                    
                    <div class="form-group">
                        <label for="fecha">* Fecha del Incidente:</label>
                        <input type="date" class="form-control" id="fecha"
                     name="fecha" title="Formato Fecha" placeholder="Ingresar Fecha Nacimiento" required
                        >
                      </div>  
                      <div class="form-group">
                          <label for="tip">Tipo de maltrato</label>
                            <small>(Obligatorio)</small>
                          <select class="form-control" id="tip" name="tip" 
                          required>
                                <option value="">Seleccionar</option>
                                <option value="Psicologico">Psicologico</option>
                                <option value="Sexual">Sexual</option>
                                <option value="Fisico">Fisico</option>
                                <option value="Economico">Economico</option>
                                

                          </select>
                          <div class="form-group">
                      <label for="cant">Cantidad de agresores</label>
                          <small>(Obligatorio)</small>
                          <input type="number" class="form-control" id="cant" name="cant" placeholder="Ingrese cantidad" min="1" required>		    
                      </div>
                      </div>
                           <div class="form-group">
                        <label for="foto">* Evidencias:</label>
                        <div class="custom-file">
                       <input type="file" class="custom-file-input" id="foto" lang="es" name="foto" required>
                        <label class="custom-file-label" for="foto">Seleccionar Archivo</label>
                        </div>
                      </div> 



                     <button type="submit" class="btn btn-primary">Crear</button>
                          
                            <button type="button" id="cerrarreg" class="btn btn-default">Cerrar</button>


                        <?= form_close(); ?>       
                                    
                        </div>
                      
                  </div>
            
      </div>
    <!-- Imagen derecha -->
      
    <img class="img-from" src="<?php echo base_url();?>/resources/img/From.png" alt="Generic placeholder image" width="600" height="600">
      
  </section>  

  </br>
  <div class="container marketing">

			<!-- Three columns of text below the carousel -->
			<br>
			<div class="row">
				<div class="col-sm-12">
  <hr class="featurette-divider">
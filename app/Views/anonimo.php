<script src="<?= base_url()?>/resources/js/jsAnonimo.js"></script>
<script>
  ruta='<?= base_url()?>';  
  $(document).ready(function() {
    doaction();
  });
</script>


<div class="contenedor1">
<figure>
	<img src="<?php echo base_url();?>/resources/img/anonimo.png" alt="">
	<div class="cap"><h3>REGISTRO ANONIMO</h3>
		<p>LOS VALORES QUE INGRESE DEBEN SER 100% REALES.</p>
		
	
	</div>
    </div>
    </figure>



	<section>

    <div class="container">
    <div class="row">
    <div class="col-md-5"><!--Inzq  -->

            <div class="card">
               <div class="card-header">
                    <h3 class="card-title">REGISTRO DE DENUNCIA ANONIMA</h3>
                </div>
                <img class="card-img-top" src="<?php echo base_url();?>/resources/img/anon.png" width="20" height="150" alt="Card image cap">
                <div class="card-body">

                  <div class="alert alert-danger"  id="error" >
                <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                    <span class="sr-only">Error:</span>
              <p id="mensaje_error"></p>
            </div>        
            <div class="alert alert-success"  id="succ" >
                <span class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></span>
                    <span class="sr-only">Correcto:</span>
              <p id="mensaje_ok"></p>
            </div>        
                   <?php $validation = \Config\Services::validation(); ?>

                  <?= form_open_multipart('#', array('id' => 'frmreg','name' => 'frmreg')) ?>
                          

                      <div class="input-group input-group-lg">
                       <span class="input-group-addon">
                        <span class="glyphicon glyphicon-user"></span>
                       
                       </span>
                      
                        
                      </div>    
                      <!-- FORMULARIO ANONIMO -->   
                             
                      <div class="form-group">
                        <label for="aliasP">* Alias:</label>
                        <input type="text" class="form-control" id="aliasP"
                         name="aliasP" title="Solo alfanumericos" placeholder="Ingresar Nombre" required>
                      </div>
                      
                      
                    <div class="form-group">
                        <label for="dir1">* Direccion y Referencia:</label>
                        <input type="text" class="form-control" id="dir1"
                     name="dir1" title="Solo alfanumericos" placeholder="Ingresar Direccion" required
                        >
                      </div>
                      <div class="form-group">
                        <label for="det1">Detalle de lo sucedido:</label>
                        <small>(Obligatorio)</small>
                        <textarea class="form-control" id="det1" name="det1"
                        rows="3" required></textarea>
                        </div>
                    
                    <div class="form-group">
                        <label for="fecha1">* Fecha del Incidente:</label>
                        <input type="date" class="form-control" id="fecha1"
                     name="fecha1" title="Formato Fecha" placeholder="Ingresar Fecha Nacimiento" required
                        >
                      </div>  
                      <div class="form-group">
                          <label for="tip1">Tipo de maltrato</label>
                            <small>(Obligatorio)</small>
                          <select class="form-control" id="tip1" name="tip1" 
                          required>
                                <option value="">Seleccionar</option>
                                <option value="Psicologico">Psicologico</option>
                                <option value="Sexual">Sexual</option>
                                <option value="Fisico">Fisico</option>
                                <option value="Economico">Economico</option>
                                

                          </select>
                          <div class="form-group">
                      <label for="cant1">Cantidad de agresores</label>
                          <small>(Obligatorio)</small>
                          <input type="number" class="form-control" id="cant1" name="cant1" placeholder="Ingrese cantidad" min="1" required>		    
                      </div>
                      </div>
                      <div class="form-group">
                        <label for="correo1">* Correo:</label>
                        <input type="email" class="form-control" id="correo1"
                     name="correo1" title="Formato correo" placeholder="Ingresar Correo" required
                        >
                      </div>



                     <button type="submit" class="btn btn-primary">Crear</button>
                          
                            <button type="button" id="cerrarreg" class="btn btn-default">Cerrar</button>


                        <?= form_close(); ?>       
                                    
                        </div>
                      
                  </div>
            
      </div>
      <!-- Imagen derecha -->
      
      <img class="img-from" src="<?php echo base_url();?>/resources/img/From.png" alt="Generic placeholder image" width="600" height="600">

</section>  

</br>
<div class="container marketing">

<!-- Three columns of text below the carousel -->
<br>
<div class="row">
<div class="col-sm-12">
<hr class="featurette-divider">
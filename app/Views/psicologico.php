<div class="contenedor">
<figure>
	<img src="<?php echo base_url();?>/resources/img/psicologica.jpg" alt="">
	<div class="cap"><h3>Violencia Psicológica</h3>
		<p>La violencia psicológica es un tipo de violencia que se ejerce sin la intervención de acciones físicas, 
        pero que afecta a la víctima no solo a nivel psicológico y emocional, sino también físico.</p>
			
	</div>
    </div>
    </figure>
	<section>

	
		<div class="container marketing">

			<!-- Three columns of text below the carousel -->
			<br>
			<div class="row">
				<div class="col-sm-8">
        <br><br><br>
          <p class="text-justify">
            La violencia psicológica, que por lo general acompaña a las otras formas de violencia y que también tiene como base la desigualdad de género, incluye varias formas de afectación tales como:
            <ul>
              <li>Amenazas</li>
              <li>Insultos relacionados con el aspecto físico de la persona</li>
              <li>Con su inteligencia</li>
              <li>Con sus capacidades como trabajadora</li>
              <li>Con su calidad como madre</li>
              <li>Esposa o ama de casa</li>
              <li>Humillaciones de todo tipo</li>
              <li>Desprecio</li>
              <li>Desvalorización de su trabajo o de sus opiniones</li>
            </ul>
          </p>
				</div>
        
				<div class="alert alert-primary col-sm-4" role="alert">
  			  <p class="text-justify">
          
            PON ATENCION!!!!
  				  <br>
  				  La violencia psicológica, que por lo general acompaña a las otras formas de violencia y que también tiene como base la
            desigualdad de género, es definida en la Ley N° 30364 como “la acción o conducta tendiente a controlar o aislar a la
            persona contra su voluntad, a humillarla o avergonzarla y que puede ocasionar daños psíquicos
          </p>
		  <div>
   <div class="circel-postion"><div class="vc_column-inner vc_custom_1543414345971"><div class="wpb_wrapper"><div id="seofy_services_5c257f89b6ea8" class="seofy_module_services_3"><div class="services_wrapper"><div class="services_icon_wrapper"><div class="seofy_hexagon"><svg style="filter: drop-shadow(4px 5px 4px rgba(255,167,5,0.3)); fill: #ffa705;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4"><path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path></svg></div><div class="seofy_hexagon"><svg style="filter: drop-shadow(4px 5px 4px rgba(255,86,0,0.3)); fill: #ff5600;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4"><path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path></svg></div><i class="services_icon flaticon-analytics"></i></div></div></div>  
</div></div></div>
</div>
				</div>

			</div>
      <br>
      <div class="c">
            <iframe width="1000" height="500" src="https://www.youtube.com/embed/RMMsZTEbxaw" title="YouTube video player" frameborder="" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
				  </div>
      <br><br><br><br>


			<div class="row">
				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/psicologica-1.jpg" alt="Generic placeholder image" width="140" height="140">
					<h2>¿Qué otros entornos afecta este tipo de abuso?</h2>
					<p class="text-justify">Nos encontramos ante un problema de salud pública que tiene un impacto no solo a nivel físico, sino también en el ámbito laboral, económico, social y familiar ...</p>
					<p><a class="btn btn-secondary" href="https://www.france24.com/es/am%C3%A9rica-latina/20210310-violencia-genero-mujeres-onu-informe	" target="_blank"   role="button">Leer más &raquo;</a></p>
				</div><!-- /.col-lg-4 -->
				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/psicologica-2.jpg" alt="Generic placeholder image" width="140" height="140">
					<h2>¿Cuáles son los factores que aumentan el riesgo de que exista esta forma de maltrato en la pareja?</h2>
					<p class="text-justify">Dado que el maltrato psicológico, al menos a un nivel bajo u ocasional, es muy común en las parejas, se enfatiza la necesidad de desarrollar acciones de prevención no solo por su fuerte impacto negativo, sino también porque no es independiente del maltrato físico ...</p>
					<p><a class="btn btn-secondary" href="https://www.scielosp.org/article/scol/2017.v13n4/611-632/" target="_blank"  role="button">Leer más &raquo;</a></p>
				</div><!-- /.col-lg-4 -->
				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/psicologica-3.jpg" alt="Generic placeholder image" width="140" height="140">
					<h2>Abuso psicológico entre parejas</h2>
					<p class="text-justify">A lo largo de la historia, la lucha por los derechos humanos ha estado muy presente. Y, aunque se ha logrado una mejoría en la equiparación de estos a nivel mundial, no ocurre lo mismo en el caso de los derechos de la mujer....</p>
					<p><a class="btn btn-secondary" href="https://www.paho.org/es/temas/violencia-contra-mujer" target="_blank"   role="button">Leer más &raquo;</a></p>
         <br><br><br><br>
				</div><!-- /.col-lg-4 -->
				
			
        
			<!-- START THE FEATURETTES -->

			<hr class="featurette-divider">
        
			<div class="row featurette">
				<div class="col-md-7">
        <br><br>
					<h2 class="featurette-heading">Abuso psicológico<span class="text-muted">
            <br>
            Cifras preocupantes...
          </span></h2>
					<p class="text-justify">
            En el Perú, según la Encuesta Demográfica y de Salud Familiar-ENDES de 2019 (INEI, 2019) el 32,2% de las mujeres ha sido, al menos una vez, víctima de una forma de violencia física y/o sexual por parte de su cónyuge o pareja, el 64,2% de una forma de violencia psicológica y/o verbal y el 60,5% de ellas manifiesta haber sido o ser el objeto de alguna forma de control o dominación.
          </p>
				</div>
				<div class="col-md-5">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/psicologica-4.jpg" alt="Generic placeholder image" width="300" height="300">
					<br><br><br><br>
				</div>
			</div>

			<hr class="featurette-divider">

			<div class="row featurette">
				<div class="col-md-7 order-md-2">
					<h2 class="featurette-heading">Mucho cuidado!!!</span></h2>
					<p class="text-justify">
            Muchas veces, el abuso psicológico puede ir cambiando lentamente a violencia física. Las situaciones de conflicto, posconflicto y desplazamiento pueden agravar la violencia existente, como la infligida contra la mujer por su pareja y la violencia sexual fuera de la pareja, y dar lugar a nuevas formas de violencia contra la mujer.
          </p>
				</div>
				<div class="col-md-5 order-md-1">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/psicologica-5.jpg" alt="Generic placeholder image" width="300" height="300">
				</div>
			</div>
  
			
<br>




</section>	

			<hr class="featurette-divider">


			<!-- /END THE FEATURETTES -->

  <div class="container marketing">

			<!-- Three columns of text below the carousel -->
			<br>
			<div class="row">
				<div class="col-sm-12">
  <hr class="featurette-divider">



			

  
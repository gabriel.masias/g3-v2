  

		<div id="myCarousel" class="carousel slide" data-ride="carousel">
			<ol class="carousel-indicators">
				<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
				<li data-target="#myCarousel" data-slide-to="1"></li>
				<li data-target="#myCarousel" data-slide-to="2"></li>
			</ol>
			<div class="carousel-inner">
				<div class="carousel-item active">
					<center><img class="first-slide" src="<?php echo base_url();?>/resources/img/1.jpeg" alt="Generic placeholder image" width="1500" height="600"></center>
					<div class="container">
						<div class="carousel-caption d-none d-md-block text-left">
							<center><h1>Violencia de hogar</h1>
							<strong>
                <h5>
                  <font color="#060507">La violencia contra la mujer -especialmente la ejercida por su pareja y la violencia 
                  sexual- constituye un grave problema de salud pública y una violación de los derechos humanos de las mujeres. 
                  La mayor parte de las veces el agresor es la pareja. En todo el mundo, casi un tercio (27%) de las mujeres de 15
                  a 49 años que han estado en una relación informan haber sufrido algún tipo de violencia física y /o sexual por 
                  su pareja.
                </font>
                </h5>
              </strong>
							<p><a class="btn btn-lg btn-primary" href="https://www.un.org/es/coronavirus/what-is-domestic-abuse" target="_blank"  role="button">Aprende más</a></p>
						</center>
						</div>
					</div>
				</div>
				<div class="carousel-item">
					<center><img class="second-slide" src="<?php echo base_url();?>/resources/img/2.png" alt="Generic placeholder image" width="1500" height="600"></center>
					<div class="container">
						<div class="carousel-caption d-none d-md-block">
							<h1>Acoso.</h1>
							<p>La violencia sexual en línea y en las calles se ha normalizado en el Perú. Plan International trabaja para cambiar esta realidad. ¡Tu ayuda es importante!. La violencia sexual en niñas y adolescentes como forma de violencia es uno de los mayores problemas que aquejan nuestra sociedad. Ellas no se sienten libres de recorrer los espacios públicos, pues consideran que son lugares inseguros. Tampoco se sienten tranquilas navegando en Internet, porque en cualquier momento un acosador puede aparecer. Muchas veces ni siquiera están bien en casa, donde se supone deberían estar seguras, pues allí sufren de abuso sexual. </p>
							<center><p><a class="btn btn-lg btn-primary" href="https://observatorioviolencia.pe/aprueban-mecanismos-para-la-erradicacion-del-acoso-contra-las-mujeres-en-la-vida-politica/" target="_blank" role="button">Aprende más</a></p>
							</center>
						</div>
					</div>
				</div>
				<div class="carousel-item">
					<center><img class="third-slide" src="<?php echo base_url();?>/resources/img/3.png" alt="Generic placeholder image" width="1500" height="600"></center>
					<div class="container">
						<div class="carousel-caption d-none d-md-block text-right">
							<center><h1>STOP.</h1>
							<strong>
                <h5>
                  <font color="#060507">La violencia contra la mujer -especialmente la ejercida por su pareja y la violencia 
                  sexual- constituye un grave problema de salud pública y una violación de los derechos humanos de las mujeres. 
                  La mayor parte de las veces el agresor es la pareja. En todo el mundo, casi un tercio (27%) de las mujeres de 15
                  a 49 años que han estado en una relación informan haber sufrido algún tipo de violencia física y /o sexual por 
                  su pareja.
                </font>
                </h5>
              </strong>
              <p><a class="btn btn-lg btn-primary" href="https://busquedas.elperuano.pe/normaslegales/ley-para-prevenir-sancionar-y-erradicar-la-violencia-contra-ley-n-30364-1314999-1/" target="_blank" role="button">Aprende más</a></p>
						</center>
						</div>
					</div>
				</div>
			</div>
			<a class="carousel-control-prev" href="#myCarousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#myCarousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>


		<!-- Marketing messaging and featurettes
		================================================== -->
		<!-- Wrap the rest of the page in another container to center all the content. -->

		<div class="container marketing">

			<!-- Three columns of text below the carousel -->
			<br>
			<div class="row">
				<div class="col-sm-8">
          <p class="text-justify">
            La violencia contra niñas y mujeres es una de las violaciones de los derechos humanos más extendidas, resistentes y devastadoras del contexto internacional actual sobre las que apenas se informa debido a la impunidad de la cual gozan los perpetradores, y el silencio, la estigmatización y la vergüenza que sufren muchas de las víctimas. De acuerdo a Naciones Unidas, una de cada tres mujeres en todo el mundo ha sufrido violencia física o sexual, principalmente por parte de un compañero sentimental (2019).
				    Aunque todas las mujeres, en todas partes del mundo, pueden sufrir violencia de género, algunas son especialmente vulnerables, ejemplo de ellas son las niñas y las mujeres adultas mayores, las mujeres que se identifican como lesbianas, bisexuales, transgénero o de alguna identidad disidente, las migrantes, desplazadas y refugiadas, las de pueblos indígenas o minorías étnicas, o mujeres y niñas que viven con alguna discapacidad.
            En aras de indagar sobre esta problemática internacional y tomando en cuenta que el próximo 25 de noviembre se celebra el Día Internacional de la Eliminación de la Violencia contra la Mujer, el Centro de Documentación del Idehpucp comparte una serie de investigaciones actuales que da luces sobre el caso peruano.
          </p>
				</div>
				<div class="alert alert-primary col-sm-4" role="alert">
  			  <p class="text-justify">
            PON ATENCION!!!!
  				  <br>
  				  De acuerdo a Naciones Unidas, una de cada tres mujeres en todo el mundo ha sufrido violencia física o sexual.
          </p>
		  <div>
   <div class="circel-postion"><div class="vc_column-inner vc_custom_1543414345971"><div class="wpb_wrapper"><div id="seofy_services_5c257f89b6ea8" class="seofy_module_services_3"><div class="services_wrapper"><div class="services_icon_wrapper"><div class="seofy_hexagon"><svg style="filter: drop-shadow(4px 5px 4px rgba(255,167,5,0.3)); fill: #ffa705;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4"><path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path></svg></div><div class="seofy_hexagon"><svg style="filter: drop-shadow(4px 5px 4px rgba(255,86,0,0.3)); fill: #ff5600;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4"><path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path></svg></div><i class="services_icon flaticon-analytics"></i></div></div></div>  
</div></div></div>
</div>
				</div>

			</div>

      <br><br><br><br>


			<div class="row">
				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/4.png" alt="Generic placeholder image" width="140" height="140">
					<h2>La violencia de género en 2021 en el mundo</h2>
					<p class="text-justify">2021 Peru. Cuatro víctimas de violencia de género en un mismo día. Y, a medida que avanza el calendario, son más. Pero la violencia contra las mujeres ...</p>
					<p><a class="btn btn-secondary" href="https://www.france24.com/es/am%C3%A9rica-latina/20210310-violencia-genero-mujeres-onu-informe	" target="_blank"   role="button">Leer más &raquo;</a></p>
				</div><!-- /.col-lg-4 -->
				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/5.png" alt="Generic placeholder image" width="140" height="140">
					<h2>¿Qué es la igualdad de género? ¿En qué consiste? </h2>
					<p class="text-justify">La igualdad de género es un concepto que cada vez encontramos más en nuestro día a día: en las noticias, en nuestros puestos de trabajo, en las redes sociales… Pero, ...</p>
					<p><a class="btn btn-secondary" href="https://www.codespa.org/blog/cat/actua/derechos-de-la-mujer/?gclid=EAIaIQobChMI34upqIuY8QIVwZuGCh2z2AA3EAAYASAAEgIXfPD_BwE   " target="_blank"  role="button">Leer más &raquo;</a></p>
				</div><!-- /.col-lg-4 -->
				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/6.png" alt="Generic placeholder image" width="140" height="140">
					<h2>Los derechos de las mujeres</h2>
					<p class="text-justify">A lo largo de la historia, la lucha por los derechos humanos ha estado muy presente. Y, aunque se ha logrado una mejoría en la equiparación de estos a nivel mundial, no ocurre lo mismo en el caso de los derechos de la mujer....</p>
					<p><a class="btn btn-secondary" href="https://www.unwomen.org/es/news/stories/2020/4/statement-ed-phumzile-violence-against-women-during-pandemic?gclid=EAIaIQobChMIo9D2wYuY8QIVwcqGCh1pJQ3IEAAYASAAEgI-o_D_BwE" target="_blank"   role="button">Leer más &raquo;</a></p>
         <br><br><br><br>
				</div><!-- /.col-lg-4 -->
				
			

			<!-- START THE FEATURETTES -->

			<hr class="featurette-divider">

			<div class="row featurette">
				<div class="col-md-7">
					<h2 class="featurette-heading">Violencia contra las mujeres peruanas en la pandemia: <span class="text-muted">Estudio y Análisis</span></h2>
					<p class="lead">Desde el 17 de marzo hasta el 31 de agosto de 2020 se atendieron en el país alrededor de 14.583 casos de violencia contra la mujer, según registros de los Equipos Itinerantes de Urgencia (EIU). Estos grupos fueron formados desde el Ministerio de la Mujer, para atender a las víctimas de violencia durante el aislamiento social obligatorio.</p>
				</div>
				<div class="col-md-5">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/7.png" alt="Generic placeholder image" width="300" height="300">
					<br><br><br><br>
				</div>
			</div>

			<hr class="featurette-divider">

			<div class="row featurette">
				<div class="col-md-7 order-md-2">
					<h2 class="featurette-heading">¿Cómo podemos contribuir al cambio?<span class="text-muted">Ciudades Seguras Para Niñas...</span></h2>
					<p class="lead">Para vencer la violencia contra las mujeres durante la pandemia y generar espacios seguros para todas las peruanas, es importante crear políticas para la prevención y atención de la violencia, el acceso a la justicia y el fortalecimiento institucional. </p>
				</div>
				<div class="col-md-5 order-md-1">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/8.png" alt="Generic placeholder image" width="300" height="300">
				</div>
			</div>
      <br><br><br><br>

			<hr class="featurette-divider">

			<div class="row featurette">
				<div class="col-md-7">
					<h2 class="featurette-heading">¿Cómo se ve la violencia de género en el mundo?<span class="text-muted">Violencia contra las mujeres durante la pandemia en 2021.</span></h2>
					<p class="lead">El aislamiento que hemos vivido desde marzo de 2020 a causa del Covid-19, no solo ha afectado fuertemente la economía del mundo. La realidad es que las niñas, adolescentes y mujeres jóvenes que viven en espacios vulnerables, se han visto obligadas a pasar más tiempo junto a sus agresores. Esta situación ha desencadenado un aumento en las estadísticas de violencia contra la mujer en el Perú 2021.</p>
				</div>
				<div class="col-md-5">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/9.png" alt="Generic placeholder image" width="300" height="300">
				</div>
			</div>
			
			</div>	
  	
			




			
<br>




</section>	

			<hr class="featurette-divider">


			<!-- /END THE FEATURETTES -->


			
<!DOCTYPE html>

<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta name="description" content="">
		<meta name="author" content="">
		<link rel="icon" href="../../../../favicon.ico">

		<title>Grupo 03</title>

		<!-- Bootstrap core CSS -->
		<link rel="stylesheet" href="<?php echo base_url();?>/dist/css/bootstrap.css">
		
		<link rel="stylesheet" href="<?php echo base_url();?>/resources/css/estilo.css">

		
<!------ Include the above in your HEAD tag ---------->



		<link rel="stylesheet" href="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.css">

		
		<!-- Custom styles for this template -->
		<!-- Bootstrap core JavaScript
		================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    	<script src="../../assets/js/vendor/holder.min.js"></script>
		<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

		<!-- ICONOS -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
		<script src="../../../../assets/js/ie10-viewport-bug-workaround.js"></script>
		<script src="https://unpkg.com/bootstrap-table@1.18.0/dist/bootstrap-table.min.js"></script>
		<script>
		 //BLOQUEAR CLICK DERECHO
		document.addEventListener('contextmenu', event => event.preventDefault());
		</script>
		
	</head>

	<body>
		    <header>
		<nav class="navbar navbar-expand-md navbar-dark  bg-dark">
			
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarsExampleDefault">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active">
						<a class="nav-link" href="<?php echo base_url();?>/">¡Despierta mujer!<span class="sr-only"></span></a>
					</li>
					
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Tipos de violencia</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
							<a class="dropdown-item" href="<?php echo base_url();?>/Home/Psicologica">Psicológica</a>
							<a class="dropdown-item" href="<?php echo base_url();?>/Home/sexual">Sexual</a>
							<a class="dropdown-item" href="<?php echo base_url();?>/Home/fisica">Física</a>
							<a class="dropdown-item" href="<?php echo base_url();?>/Home/economica">Económica</a>
							<!--
              <a class="dropdown-item" href="<?php echo base_url();?>/Home/desigualdad">Desigualdad y discriminacion</a>
							<a class="dropdown-item" href="<?php echo base_url();?>/Home/patrimonial">Patrimonial</a>
							-->
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Servicios</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
							<a class="dropdown-item" href="<?php echo base_url();?>/Anonimo">Registrar Denuncia Anónima</a>	


              <?php
						  $session = \Config\Services::session(); 
						   if($session->has('usuario')){
							 	?>	
							 <a href="<?php echo base_url();?>/Privado" class="dropdown-item">Registrar Denuncia Privada</a>	 	
                
            


             <?php
						  }	else{
						  	?>
						  	
						  	<div class="input-group-append">
								
							</div>
						  <?php
						  } 	
						  ?>            
            
            </div>
					</li>

           <?php
						  $session = \Config\Services::session(); 
						   if($session->has('usuario')){
							 	?>	
							 <li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="http://example.com" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Detalles Registros</a>
						<div class="dropdown-menu" aria-labelledby="dropdown01">
							<a class="dropdown-item" href="<?php echo base_url();?>/ListarAnonimo">Denuncias Anónimas</a>
							<a class="dropdown-item" href="<?php echo base_url();?>/ListarPrivado">Denuncias Privadas</a>
						</div>
					</li>


             <?php
						  }	else{
						  	?>
						  	
						  	<div class="input-group-append">
								
							</div>
						  <?php
						  } 	
						  ?>


          
					<li class="nav-item">
						<a class="nav-link" href="<?php echo base_url();?>/Home/Contacto">Contáctanos</a>
					</li>
          <li class="nav-item">
						<a class="nav-link" href="<?php echo base_url();?>/Home/QuienesSomos">¿Quiénes somos?</a>
					</li>
					
				

					</ul> 
					
					
					<?php
						  $session = \Config\Services::session(); 
						   if($session->has('usuario')){
                echo('Bienvenido, ');
							 	echo $session->get('usuario');
                 
                
							 	?>
                 &nbsp;	
                 &nbsp;	
							 <a href="<?php echo base_url();?>/login/cerrarsesion" class="btn btn-outline-success my-2 my-sm-0" >Cerrar Sesión</a>	 	
							 &nbsp;	
							  	
						 <?php
						  }	else{
						  	?>
						  	<a href="<?php echo base_url();?>/login/index" class="btn btn-outline-success my-2 my-sm-0" <span class="input-group-text"><i class="fas fa-user"></i></span>  Iniciar Sesión</a>	
						  	<div class="input-group-append">
								
							</div>
						  <?php
						  } 	
						  ?>








              
					

					
				</form>	
						  
						  
						 	
					
				
			
			</div>
		</nav>
	</header>

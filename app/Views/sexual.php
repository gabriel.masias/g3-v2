<div class="contenedor">
<figure>
	<img src="<?php echo base_url();?>/resources/img/sexual.jpg" alt="">
	<div class="cap"><h3>Violencia Sexual</h3>
		<p>Si tú no das tu permiso o si alguien te obliga a hacer algo sexual, 
            eso es agresión sexual, abuso y/o violación. Que alguien te haga daño de alguna de estas maneras nunca es tu culpa. </p>
		
	
	</div>
    </div>
    </figure>
    
	</br>
  <div class="container marketing">

			<!-- Three columns of text below the carousel -->
	<div class="container marketing">

			<!-- Three columns of text below the carousel -->
			<br>
			<div class="row">
				<div class="col-sm-8">
          <p class="text-justify">
            El “terrorismo íntimo” expresado a través de escenas extremadamente violentas y regulares, así como de relaciones sexuales forzadas, motiva más intensamente a las mujeres a vencer su temor y denunciar estas violencias, a diferencia de la “violencia situacional” donde las mujeres dudan, a menudo, en acudir a un puesto policial.
            En ese sentido, pueden considerarse casos de violencia sexual el acoso sexual, los tocamientos indebidos, la pornografía coaccionada, las violaciones sexuales, entre otros.
          </p>
          <div class="c">
            <img class="w3-round-xxlarge" alt="Norway" style="width:100%" src="<?php echo base_url();?>/resources/img/sexual-6.jpg" height="400">
				  </div>
				</div>
				<div class="alert alert-primary col-sm-4" role="alert">
  			  <p class="text-justify">
            PON ATENCION!!!!
  				  <br>
  				  Según las cifras del Ministerio de la Mujer y Poblaciones Vulnerables (MIMP), hasta octubre del 2019 se registraron 13,520 casos de mujeres afectadas por violencia sexual atendidas en los Centro de Emergencia Mujer. 9,126 de las mujeres afectadas eran niñas y adolescentes entre los 0 y 17 años de edad mientras que el 4,394 restante eran mujeres adultas hasta los 59 años.
          </p>
		  <div>
   <div class="circel-postion"><div class="vc_column-inner vc_custom_1543414345971"><div class="wpb_wrapper"><div id="seofy_services_5c257f89b6ea8" class="seofy_module_services_3"><div class="services_wrapper"><div class="services_icon_wrapper"><div class="seofy_hexagon"><svg style="filter: drop-shadow(4px 5px 4px rgba(255,167,5,0.3)); fill: #ffa705;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4"><path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path></svg></div><div class="seofy_hexagon"><svg style="filter: drop-shadow(4px 5px 4px rgba(255,86,0,0.3)); fill: #ff5600;" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 177.4 197.4"><path d="M0,58.4v79.9c0,6.5,3.5,12.6,9.2,15.8l70.5,40.2c5.6,3.2,12.4,3.2,18,0l70.5-40.2c5.7-3.2,9.2-9.3,9.2-15.8V58.4 c0-6.5-3.5-12.6-9.2-15.8L97.7,2.4c-5.6-3.2-12.4-3.2-18,0L9.2,42.5C3.5,45.8,0,51.8,0,58.4z"></path></svg></div><i class="services_icon flaticon-analytics"></i></div></div></div>  
</div></div></div>
</div>
				</div>

			</div>
      <br><br><br>
      <div class="c">
        <iframe width="1000" height="500" src="https://www.youtube.com/embed/NAQq6Q2Pob0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
			</div>

      <br><br><br><br>


			<div class="row">
				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/sexual-1.jpg" alt="Generic placeholder image" width="140" height="140">
					<h2>"Ley para prevenir, sancionar y erradicar la violencia contra las mujeres y los integrantes del grupo familiar"</h2>
					<p class="text-justify">En los últimos años, el Estado peruano reguló nuevas fórmulas normativas con la consigna de reducir los casos de todos los tipos de violencia que afectan a las mujeres y a las niñas; desde la violencia psicológica hasta los feminicidios ...</p>
					<p><a class="btn btn-secondary" href="http://www.mimp.gob.pe/files/transparencia/ley-30364.pdf" target="_blank"   role="button">Leer más &raquo;</a></p>
				</div><!-- /.col-lg-4 -->
				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/sexual-2.jpg" alt="Generic placeholder image" width="140" height="140">
					<h2>Cientos y cientos de mujeres son abusadas sexualmente al año</h2>
					<p class="text-justify">La violencia sexual y feminicida afecta a cientos de niñas y mujeres en Perú cada año. Solo en 2019, el Estado contabilizó 166 feminicidios, 570 tentativas de feminicidio y casi 17.000 casos de violencia sexual contra mujeres de todas las edades. Desde que empezó el 2020, 30 ya fueron asesinadas por ser mujeres y más de 1.500, fueron violentadas sexualmente. ...</p>
					<p><a class="btn btn-secondary" href="https://www.france24.com/es/20200308-peru-dia-internacional-mujer-retrocesos-lucha-genero" target="_blank"  role="button">Leer más &raquo;</a></p>
				</div><!-- /.col-lg-4 -->
				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/sexual-3.jpg" alt="Generic placeholder image" width="140" height="140">
					<h2>¿Qué podemos hacer para evitar el abuso sexual? </h2>
					<p class="text-justify">Las madres, los padres y cuidadores(as) deben hacer cinco cosas importantes para prevenir el abuso: ENSEÑAR ASERTIVIDAD, RESPETAR, SABER ESCUCHAR, ESTAR SIEMPRE INFORMADOS E INFORMADAS, HABLAR CLARO SOBRE SEXUALIDAD, ADVERTIR SOBRE EL ABUSO SEXUAL ....</p>
					<p><a class="btn btn-secondary" href="https://www.mimp.gob.pe/files/programas_nacionales/pncvfs/libro_abusosexual.pdf" target="_blank"   role="button">Leer más &raquo;</a></p>
         <br><br><br><br>
				</div><!-- /.col-lg-4 -->
				
			
        
			<!-- START THE FEATURETTES -->

			<hr class="featurette-divider">
        
			<div class="row featurette">
				<div class="col-md-7">
        <br><br>
					<h2 class="featurette-heading">El abuso sexual es un tipo de actividad o contacto sexual en el que no das tu consentimiento<span class="text-muted">
          </span></h2>
					<p class="text-justify">
            Un atacante puede usar la fuerza física o amenazas, o darle drogas o alcohol a su víctima para abusarla sexualmente. El abuso sexual incluye violación y coerción sexual. En los Estados Unidos, una de cada tres mujeres sufrió algún tipo de violencia sexual.1 Si sufriste un ataque sexual, no es tu culpa, no importa en qué circunstancias haya ocurrido.
          </p>
				</div>
				<div class="col-md-5">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/sexual-4.jpg" alt="Generic placeholder image" width="300" height="300">
					<br><br><br><br>
				</div>
			</div>

			<hr class="featurette-divider">

			<div class="row featurette">
				<div class="col-md-7 order-md-2">
					<h2 class="featurette-heading">¿Cómo puedo ayudar a una persona que fue abusada sexualmente?</span></h2>
					<p class="text-justify">
            Puedes ayudar a una amiga o familiar que fue abusada sexualmente si la escuchas y las consuelas. Recuérdale que le crees lo que dice. Refuerza el mensaje de que no es su culpa. Una víctima nunca causa un abuso sexual ni "se lo busca". También puedes explicarle que es natural sentirse confundida, tener problemas para recordar lo que ocurrió o sentirse enojada, bloqueada o avergonzada.
            Pregúntale si quiere que la acompañes al hospital o a ver a un terapeuta. Si decide denunciar el delito a la policía, pregúntale si quiere que la acompañes. Dile que puede recibir ayuda. Infórmale sobre las líneas de ayuda a las que puede llamar para hablar con alguien. Aquí puedes ver más consejos para ayudar a una persona que sufrió un ataque o abuso sexual.
          </p>
				</div>
				<div class="col-md-5 order-md-1">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/sexual-5.jpg" alt="Generic placeholder image" width="300" height="300">
				</div>
			</div>
  
			
<br>




</section>	

			<hr class="featurette-divider">


			<!-- /END THE FEATURETTES -->


  <div class="container marketing">

			<!-- Three columns of text below the carousel -->
			<br>
			<div class="row">
				<div class="col-sm-12">
  <hr class="featurette-divider">

<div class="contenedor">
<figure>
	<img src="<?php echo base_url();?>/resources/img/economica.jpg" alt="">
	<div class="cap"><h3>Violencia Economica</h3>
		<p>La violencia económica es una forma de violencia doméstica. Tanto la retención de dinero, 
		el robo de dinero así como el restringirle el uso de los recursos económicos son ejemplos de abuso económico.</p>
		
	
	</div>
    </div>
    </figure>
	</br>
 

			<!-- Three columns of text below the carousel -->
			<br>

  			<!-- Three columns of text below the carousel -->
	<div class="container marketing">

			<!-- Three columns of text below the carousel -->
			<br>
			<div class="row">
				<div class="col-sm-7">
          <p class="text-justify">
          <strong>Violencia económica</strong> es una forma de abuso cuando una de las dos partes implicadas en una pareja tiene control sobre la otra en el acceso a los recursos económicos, lo que disminuye la capacidad de la víctima de mantenerse a sí misma y la obliga a depender financieramente del perpetrador.
          </p>
          <div class="c">
            <img class="w3-round-xxlarge" alt="Norway" style="width:100%" src="<?php echo base_url();?>/resources/img/economica-1.jpg" height="400">
				  </div>
				</div>
				<div class="alert alert-primary col-sm-5" role="alert">
  			  <p class="text-justify">
            PON ATENCION!!!!
  				  <br>
  				<iframe width="425" height="350" src="https://www.youtube.com/embed/WDjwuYTPtO4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </p>
		
				</div>

			</div>

      <br><br><br><br>


			<div class="row">
				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/economica-2.jpg" alt="Generic placeholder image" width="140" height="140">
					<h2>El abuso económico en una situación doméstica puede incluir:</h2>
					<p class="text-justify">
		
			<ul> 
              <li>
              	Impedir a un cónyuge la adquisición de recursos, tales como restringir su habilidad para encontrar empleo, mantener o avanzar en sus carreras y adquirir valor</li>
              <li> Impedir a la víctima la obtención de educación</li>
              <li>Explotar los recursos económicos de la víctima</li>
              
            </ul></p>
					<p><a class="btn btn-secondary" href=https://es.wikipedia.org/wiki/Abuso_econ%C3%B3mico target="_blank"   role="button">Leer más &raquo;</a></p>
				</div><!-- /.col-lg-4 -->
				
				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/economica-4.jpg" alt="Generic placeholder image" width="140" height="140">
					<h2>La violencia económica y patrimonial en la Ley N° 30364</h2>
					<p class="text-justify">
			<ul> 
             	En nuestro país, la Ley para prevenir, sancionar y erradicar la
				violencia contra las mujeres y los integrantes del grupo familiar, Ley N°
				30364, publicada en el diario oficial El Peruano con fecha 23 de noviembre de 2015, solo establece los supuestos de violencia económica y
				patrimonial en forma conjunta.
              
            </ul></p>
					<p><a class="btn btn-secondary" href=https://www.unife.edu.pe/facultad/derecho/familia/publicaciones/REVISTA_PERSONA_Y_FAMILIA_2017/LA%20VIOLENCIA%20ECON%C3%93MICA%20Y.O%20PATRIMONIAL%20CONTRA%20LAS%20MUJERES%20EN%20EL%20%C3%81MBITO%20FAMILIAR.pdf target="_blank"   role="button">Leer más &raquo;</a></p>
				</div>

				<div class="col-lg-4">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/economica-3.jpg" alt="Generic placeholder image" width="140" height="140">
					<h2>Casos típicos de violencia económica dentro del ámbito familiar</h2>
					<p class="text-justify">
						Estos testimonios son casos típicos de violencia económica ejercida
			contra la mujer. A continuación veremos supuestos típicos de violencia
			económica dentro del ámbito familiar que padece la mujer como parte débil
			muchas veces de la relación de pareja. Sin ser los únicos, ni pretender mostrar una lista exhaustiva, presentamos a continuación los siguientes supuestos que se pueden tomar como ejemplos:
					</p>
					<p><a class="btn btn-secondary" href=https://www.unife.edu.pe/facultad/derecho/familia/publicaciones/REVISTA_PERSONA_Y_FAMILIA_2017/LA%20VIOLENCIA%20ECON%C3%93MICA%20Y.O%20PATRIMONIAL%20CONTRA%20LAS%20MUJERES%20EN%20EL%20%C3%81MBITO%20FAMILIAR.pdf target="_blank"   role="button">Leer más &raquo;</a></p>
         <br><br><br><br>
				</div><!-- /.col-lg-4 -->
				
			
        
			<!-- START THE FEATURETTES -->

			<hr class="featurette-divider">
        
			<div class="row featurette">
				<div class="col-md-7">
        <br><br>
					<h2 class="featurette-heading">¿Cómo podemos prevenir la violencia económica?<span class="text-muted">
          </span></h2>
					<p class="text-justify">
           Animar a las mujeres jóvenes a ser económicamente independientes, es darles herramientas para evitar que sean víctimas de violencia de género. Educar a los varones para entender que una pareja sana comparte la administración de los recursos con equidad y equilibrio.
          </p>
				</div>
				<div class="col-md-5">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/economica-6.jpg" alt="Generic placeholder image" width="300" height="300">
					<br><br><br><br>
				</div>
			</div>

			<hr class="featurette-divider">

			<div class="row featurette">
				<div class="col-md-7 order-md-2">
					<h2 class="featurette-heading">¿Cuáles son algunas de las protecciones económicas que puedo tomar?</span></h2>
					<p class="text-justify">
          	<ul>
							<li>Actas de nacimiento y las tarjetas de números del Seguro Social de usted y de toda su familia</li>
							<li>Copias de su cuenta bancaria, cuenta de ahorros y de los números de sus tarjetas de crédito</li>
							<li>Copias de todos las acciones o fondos de inversión</li>
							<li>Sus declaraciones de impuestos de los últimos dos años</li>
						</ul>
          </p>
				</div>
				<div class="col-md-5 order-md-1">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/economica-7.png" alt="Generic placeholder image" width="300" height="300">
				</div>
			</div>
  
			
<br>




</section>	

<div class="container marketing">

<!-- Three columns of text below the carousel -->
<br>
<div class="row">
	<div class="col-sm-12">
<hr class="featurette-divider">

<div class="contenedor">
<figure>
	<img src="<?php echo base_url();?>/resources/img/fisica.jpg" alt="">
	<div class="cap"><h3>Violencia Fisica</h3>
		<p>La violencia es un tipo de interacción entre individuos o grupos, presente en el reino animal, 
            por medio de la cual un animal o grupo de animales, intencionalmente causa daño o impone una situación, a otro u otros. </p>
		
	
	</div>
    </div>
    </figure>
    </br>
  <div class="container marketing">

			<!-- Three columns of text below the carousel -->
			<br>

  			<!-- Three columns of text below the carousel -->
	<div class="container marketing">

			<!-- Three columns of text below the carousel -->
			<br>
			<div class="row">
				<div class="col-sm-7">
          <p class="text-justify">
           El abuso físico puede incluir sacudones, quemaduras, asfixia, tirón de cabello, golpes, bofetadas, patadas y cualquier tipo de daño con un arma, como una pistola o un cuchillo. También puede incluir amenazas a tu integridad física, la de tus hijos, mascotas o familiares. El abuso físico también puede incluir restricción de la libertad contra tu voluntad, amarrándote o encerrándote en un espacio. El abuso físico en una relación íntima (romántica o sexual) también se llama violencia doméstica.
          </p>
          <div class="c">
            <img class="w3-round-xxlarge" alt="Norway" style="width:100%" src="<?php echo base_url();?>/resources/img/fisico-1.jpg" height="400">
				  </div>
				</div>
				<div class="alert alert-primary col-sm-5" role="alert">
  			  <p class="text-justify">
            PON ATENCION!!!!
  				  <br>
  				  				 <iframe width="425" height="350" src="https://www.youtube.com/embed/401cPNK31mI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
          </p>

	
				</div>

			</div>

      <br><br><br><br>


			<div class="row">
				<div class="col-lg-6">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/fisico-2.jpg" alt="Generic placeholder image" width="140" height="140">
					<h2>¿Qué es el abuso físico?</h2>
					<p class="text-justify">
			<ul>
              <li>
              	<strong>Un delito:</strong> El abuso físico es un delito, ya sea que suceda dentro o fuera de la familia o de una relación íntima. La policía tiene el poder y la autoridad de protegerte del ataque físico.</li>
              <li> <strong>Peligroso:</strong> Las víctimas cuyas parejas abusan físicamente de ellas corren mayor riesgo de sufrir heridas graves, incluso la muerte.</li>
              
            </ul></p>
					<p><a class="btn btn-secondary" href=https://espanol.womenshealth.gov/relationships-and-safety/other-types/physical-abuse target="_blank"   role="button">Leer más &raquo;</a></p>
				</div><!-- /.col-lg-4 -->
				
				<div class="col-lg-6">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/fisico-3.jpg" alt="Generic placeholder image" width="140" height="140">
					<h2>¿Cómo salgo de una relación abusiva físicamente? </h2>
					<p class="text-justify">Si estás pensando en salir de una relación abusiva, incluso si no sales de inmediato, crear un plan de seguridad puede ayudarte a saber qué hacer si tu pareja abusa de tí de nuevo. Puede ayudarte a ser más independiente cuando te marches.</p>
					<p><a class="btn btn-secondary" href=https://espanol.womenshealth.gov/relationships-and-safety/other-types/physical-abuse target="_blank"   role="button">Leer más &raquo;</a></p>
         <br><br><br><br>
				</div><!-- /.col-lg-4 -->
				
			
        
			<!-- START THE FEATURETTES -->

			<hr class="featurette-divider">
        
			<div class="row featurette">
				<div class="col-md-7">
        <br><br>
					<h2 class="featurette-heading">¿Cómo se ve la violencia de género en el mundo?<span class="text-muted">
          </span></h2>
					<p class="text-justify">
            Desde hace mucho tiempo la violencia contra las estadísticas de violencia en el Perú en niñas y mujeres se ha establecido como un problema que urge resolver. Sobre todo, en países de Latinoamérica, donde las cifras son bastante altas y representan una violación a los derechos humanos. 
          </p>
				</div>
				<div class="col-md-5">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/fisico-4.jpg" alt="Generic placeholder image" width="300" height="300">
					<br><br><br><br>
				</div>
			</div>

			<hr class="featurette-divider">

			<div class="row featurette">
				<div class="col-md-7 order-md-2">
					<h2 class="featurette-heading">¿De qué modo repercute el abuso físico sobre la salud de las mujeres a largo plazo?</span></h2>
					<p class="text-justify">
           El abuso físico puede tener efectos perdurables en tu salud física y mental. El abuso físico puede causar muchos (y duraderos) problemas de salud, que incluyen problemas cardíacos, hipertensión y problemas digestivos.1 Las mujeres abusadas tienen más posibilidades de desarrollar depresión, ansiedad o trastornos de la conducta alimentaria. Las mujeres abusadas también pueden recurrir al alcohol o a las drogas como una forma de lidiar con la situación.
          </p>
				</div>
				<div class="col-md-5 order-md-1">
					<img class="rounded-circle" src="<?php echo base_url();?>/resources/img/fisico-5.jpg" alt="Generic placeholder image" width="300" height="300">
				</div>
			</div>
  
			
<br>




</section>	

<div class="container marketing">

<!-- Three columns of text below the carousel -->
<br>
<div class="row">
	<div class="col-sm-12">
<hr class="featurette-divider">

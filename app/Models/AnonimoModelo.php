<?php namespace App\Models;

use CodeIgniter\Model;

class AnonimoModelo extends Model
{


  public function listar()
  {
	$db = \Config\Database::connect();
	$sql = "CALL sp_ListarDenunciasAnonimas()";
	$result=$db->query($sql);
	$db->close();
	return $result->getResultArray();   


}
     public function registrarAnonimo($data)
    {
    	$db = \Config\Database::connect();
    	$sql = "CALL sp_AgregarDenunciaAnonima (?,?,?,?,?,?,?,@s)";
    	$db->query($sql,$data);
    	$res =$db->query('select @s as out_param');
    	$db->close();
    	return   $res->getRow()->out_param;    
    }
}
<?php namespace App\Controllers;
use CodeIgniter\Controller;
use App\Models\PrivadoModelo;
class Privado extends BaseController
{

	public function __construct() {
     	  helper(['form', 'url']);
        
       	    	            	
    }
	public function index()
	{
		return view('header').view('Privado').view('footer');
	}
   

public function doSave()
{
  $validation =  \Config\Services::validation();
  $respuesta = array();
        
  
    $input = $this->validate([
      'docid' => [
        'rules'  => 'required|exact_length[8]',
        'errors' => [
            'required' => 'El campo dni es obligatorio',
            'exact_length' => 'El DNI debe ser de 8 digitos'
          ]
        ],

        'nombre' => [
            'rules' => 'required|min_length[2]|max_length[30]|string',
            'errors' =>[
                'required' => 'El campo nombre es obligatorio',
                'min_length' => 'El nombre no debe ser menor de 2 caracteres',
                'max_length' => 'El nombre no debe ser mayor de 30 caracteres',
                'string' => 'El nombre debe respetar reglas de puntuación'
                ]
            ]		,
            'apel' => [
                'rules' => 'required|min_length[2]|max_length[50]|string',
                'errors' =>[
                    'required' => 'El campo apellido es obligatorio',
                    'min_length' => 'El apellido no debe ser menor de 2 caracteres',
                    'max_length' => 'El apellido no debe ser mayor de 50 caracteres',
                    'string' => 'El apellido no debe tener caracteres no letras',
                    ]
                ]			,
              'cel' => [
          'rules' => 'required|min_length[8]|max_length[9]
|numeric',
          'errors' =>[
              'required' => 'El campo numero celular es obligatorio',
              'min_length' => 'El numero celular no debe ser menor de 8 caracteres',
              'max_length' => 'El numero celular no debe ser mayor de 9 caracteres',
              'numeric' => 'El numero celular no debe tener caracteres que no sean digitos'
              ]
          ]	,
          'correo' => [
          'rules' => 'required|min_length[5]|max_length[30]
|valid_email',
          'errors' =>[
              'required' => 'El campo correo eletronico es obligatorio',
              'min_length' => 'El correo eletronico no debe ser menor de 5 caracteres',
              'max_length' => 'El correo eletronico no debe ser mayor de 30 caracteres',
              'valid_email' => 'El correo eletronico debe respetar el formato de un '
              ]
          ]	

,
           
     'det' => [
    'rules' => 'required|min_length[10]|max_length[400]|string',
    'errors' =>[
      'required' => 'El campo detalles de lo ocurrido es obligatorio',
      'min_length' => 'El campo detalles de lo ocurrido no debe ser menor de 10 caracteres',
      'max_length' => 'El campo detalles de lo ocurrido no debe ser mayor de 400 caracteres',
      'string' => 'El campo detalles de lo ocurrido debe respetar las reglas de puntuación'
      ]
    ]	

,
        'tip' => [
          'rules' => 'required|in_list[Psicologico, Sexual, Fisico, Economico]',
          'errors' =>[
              'required' => 'El campo tipo de maltrato es obligatorio',
              'in_list' => 'El campo tipo de maltrato debe ser los datos de seleccion'
              ]
          ]	
        


,
    'cant' => [
    'rules' => 'required|integer|less_than[31]|greater_than[0]',
    'errors' =>[
      'required' => 'El campo Cantidad de Agresores es obligatorio',
      'integer' => 'El campo Cantidad de Agresores debe ser los datos de seleccion',
      
       'greater_than' => 'El campo Cantidad de Agresores no debe ser menor a 1 ',
      
      
      ]
    ]	

,
       'fecha' => [
         'rules'=> 'required',
         'errors' => [
           'required' => 'La fecha del incidente es obligatoria'
         ]
       ]
       ,
               'dir' => [
                'rules' => 'required|min_length[10]|max_length[200]
            |string',
                'errors' =>[
                    'required' => 'El campo direccion es obligatorio',
                    'min_length' => 'El campo direccion no debe ser menor de 10 caracteres',
                    'max_length' => 'El campo direccion no debe ser mayor de 200 caracteres',
                    'string' => 'El campo direccion debe respetar caracteres alfa numericos'
                    
                    ]
                ]

             ,

       'foto' =>[
         'uploaded[foto]',
         'mime_in[foto,image/jpg,image/jpeg,image/png,application/pdf,application/octet-stream,video/mp4,audio/mpeg3,audio/mp3]',
         'max_size[foto,1024]',
         'errors' =>[
           'uploaded' => 'No se envio un archivo',
           'mime_in' => 'No se envio uun formato aceptado(Fotos: .jpg, .png, .jpeg
           - Videos: .mp4, .avi
           - Audio: .mp3
           - Documento: .docx, .pdf)',
           'max_size' => 'El archivo no debe exceder de 1MB'
         ]
       ]
]);



          if (!$input) {
       	 $respuesta['error'] = $this->validator->listErrors() ;
  
        }  else {
                $request =  \Config\Services::request();
                $dni= $request->getPostGet('docid') ;
              $nombre= $request->getPostGet('nombre') ;
              $apel= $request->getPostGet('apel') ;
              $tel= $request->getPostGet('cel') ;
              $corr= $request->getPostGet('correo') ;
              $detall= $request->getPostGet('det') ;
              $tipo= $request->getPostGet('tip') ;
              $cantidad= $request->getPostGet('cant') ; 
              $fec= $request->getPostGet('fecha') ;
              $direcc= $request->getPostGet('dir') ;
                $img = $this->request->getFile('foto'); 
               $fot = $img->getRandomName();
                $img->move(ROOTPATH.'resources/evidencias/privado',$fot);
              $data = array($dni,$nombre,$apel,$tel,$corr,$detall,$tipo,$cantidad,$fec,$direcc,
                $fot); 
              $modelo = new PrivadoModelo($db); 
               if($modelo->registrarPersona($data)){
                 $respuesta['error']="";
                  $respuesta['ok'] = "Operacion realizada";
              }else{
                  $respuesta['error'] = "Problemas al realizar operacion!!";
              }
                 

            
        }       

          header('Content-Type: application/x-json; charset=utf-8');
          echo(json_encode($respuesta));
		

	}
	//--------------------------------------------------------------------
public function doList(){
    $respuesta=array();         
     $modelo = new PrivadoModelo($db);   
     $respuesta['data']=$modelo->listar();
     header('Content-Type: application/x-json; charset=utf-8');
        echo(json_encode($respuesta));
}

}

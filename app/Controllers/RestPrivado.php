<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
class RestPrivado extends ResourceController
{
    protected $modelName = 'App\Models\PrivadoModelo';
    protected $format    = 'json';

	public function __construct() {
     	  helper(['form', 'url']);
       	    	            	
    }
	public function extData()
	{
		return $this->formatoMensaje($this->model->listar(),"Listar",200);
	}
  public function sendData()
  {
    $validation = \Config\Services::validation();
    if (!$this->validate('regValPrivado'))
    {
      return $this->formatoMensaje(null,$validation->getErrors(),500);
    }else{
      //return $this->formatoMensaje(null,"ok",200);
      $request =  \Config\Services::request();
      $docid= $request->getPostGet('docid') ;
      $nombre= $request->getPostGet('nombre') ;
      $apel= $request->getPostGet('apel') ;
      $cel= $request->getPostGet('cel') ;
      $correo= $request->getPostGet('correo') ;
      $det= $request->getPostGet('det') ;
      $tip= $request->getPostGet('tip') ;
      $cant= $request->getPostGet('cant') ;
      $fecha= $request->getPostGet('fecha') ;
      $dir= $request->getPostGet('dir') ;
      $img = $this->request->getFile('foto'); 
      $foto = $img->getRandomName();
      $img->move(ROOTPATH.'resources/evidencias/privado',$foto); 
      $data = array($docid,$nombre,$apel,$cel,$correo,$det,$tip,$cant,$fecha,$dir,$foto); 
      if($this->model->registrarPersona($data)){
        return $this->formatoMensaje(null,"Operación Realizada con éxito",200);
      }else{
        return $this->formatoMensaje(null,"Problemas al realizar operación!!",500);
      }
    }
  }
  public function formatoMensaje($data, $mensaje, $code)
  {
    if ($code==200){
      return $this->respond(
        array('datos'=>$data,'msg'=>$mensaje,'code'=>$code)
      ); 
    }else{
      return $this->respond(
        array('msg'=>$mensaje,'code'=>$code)
      );
    }
  }
}

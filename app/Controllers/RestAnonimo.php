<?php

namespace App\Controllers;
use CodeIgniter\RESTful\ResourceController;
class RestAnonimo extends ResourceController
{
	protected $modelName = 'App\Models\AnonimoModelo';
    protected $format    = 'json';

	public function __construct() {
     	  helper(['form', 'url']);
       	    	            	
    }

    public function extData()
	{
		//return $this->respond($this->model->listar());
		return $this->formatoMensaje($this->model->listar(),"listado",200);
	}
	public function sendData()
    {
    	$validation =  \Config\Services::validation();
    	if (!$this->validate('regValAnonimo')) {
    		//error	
    		return $this->formatoMensaje(null,$validation->getErrors(),500);
    	}else{
    		//return $this->formatoMensaje(null,"ok",200);

    		 
              $dir1= $request->getPostGet('dir1') ;
              $det1= $request->getPostGet('det1') ;
              $tip1= $request->getPostGet('tip1') ; 
              $cant1= $request->getPostGet('cant1') ; 
              $fecha1= $request->getPostGet('fecha1') ;
              $img = $this->request->getFile('foto1'); 
               $fot = $img->getRandomName();
                $img->move(ROOTPATH.'resources/profiles',$fot);
                $data = array($dir1,$det1,$tip1,$cant1,$fecha1,$foto1); 
                if($this->model->registrar($data)){
               		 return $this->formatoMensaje(null,"Operacion Realizada",200);

              }else{
                   return $this->formatoMensaje(null,"Problemas al realizar operacion!!",500);
              }
    }
  }
  public function formatoMensaje($data, $mensaje, $code)
  {
    if ($code==200){
      return $this->respond(
        array('datos'=>$data,'msg'=>$mensaje,'code'=>$code)
      ); 
    }else{
      return $this->respond(
        array('msg'=>$mensaje,'code'=>$code)
      );
    }
  }
}
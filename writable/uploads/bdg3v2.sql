-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 18, 2021 at 06:22 AM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bdg3v2`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_AgregarDenuncia` (IN `v_docid` INT(8), IN `v_Nombre` VARCHAR(30), IN `v_Apellido` VARCHAR(50), IN `v_Celular` INT(9), IN `v_Correo` VARCHAR(30), IN `v_Detalle` VARCHAR(400), IN `v_TipoMaltrato` VARCHAR(12), IN `v_CantidadAgresores` INT(2), IN `v_FechaIncidente` DATE, IN `v_DireccionSuceso` VARCHAR(200), IN `v_Evidencia` VARCHAR(200), OUT `v_res` INT)  BEGIN
declare exit handler for SQLEXCEPTION
begin
rollback;
set v_res=false;
end;
start transaction;
INSERT INTO Denuncia (docid,Nombre,Apellido,Celular,Correo,Detalle,TipoMaltrato,CantidadAgresores,FechaIncidente,DireccionSuceso,Evidencia) VALUES 
(v_docid,v_Nombre,v_Apellido,v_Celular,v_Correo,v_Detalle,v_TipoMaltrato,v_CantidadAgresores,v_FechaIncidente,v_DireccionSuceso,v_Evidencia);
commit;
set v_res=true;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_BuscarDenuncia` (`v_docid` INT(8))  SELECT idDenuncia v1, docid v2, Nombre v3, Apellido v4, Celular v5, Correo v6, Detalle v7, FechaIncidente v8, TipoMaltrato v9, CantidadAgresores v10, DireccionSuceso v11 FROM denuncia Where v_docid = docid$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_ListarDenuncias` ()  SELECT idDenuncia v1, docid v2, Nombre v3, Apellido v4, Celular v5, Correo v6, Detalle v7, FechaIncidente v8, TipoMaltrato v9, CantidadAgresores v10, DireccionSuceso v11 FROM denuncia$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_listar_accesos` (IN `v_id_user` INT)  BEGIN
select p.idpagina v1 ,concat
(p.controlador,p.metodo) v2
from accesos a inner join paginas p on a.idpagina=p.idpagina 
inner join tipo_usuario tp on tp.id_tipo=a.id_tipo
inner join usuario u on u.id_tipo=tp.id_tipo
where u.id_user=v_id_user and a.estado=1;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `sp_validarUsuario` (IN `v_user` CHAR(20), IN `v_clave` TEXT)  BEGIN
select id_user v1, login v2, clave v3,id_tipo v4, descripcion v5 from  usuario

where login=v_user;

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `accesos`
--

CREATE TABLE `accesos` (
  `idacceso` int(10) UNSIGNED NOT NULL,
  `idpagina` int(10) UNSIGNED NOT NULL,
  `estado` smallint(5) UNSIGNED NOT NULL,
  `id_tipo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `accesos`
--

INSERT INTO `accesos` (`idacceso`, `idpagina`, `estado`, `id_tipo`) VALUES
(8, 1, 1, 4),
(9, 2, 1, 4),
(10, 3, 1, 4),
(11, 4, 1, 4),
(12, 1, 1, 5),
(13, 3, 1, 5);

-- --------------------------------------------------------

--
-- Table structure for table `denuncia`
--

CREATE TABLE `denuncia` (
  `idDenuncia` int(11) NOT NULL,
  `docid` int(8) DEFAULT NULL,
  `Nombre` varchar(30) DEFAULT NULL,
  `Apellido` varchar(50) DEFAULT NULL,
  `Celular` int(9) NOT NULL,
  `Correo` varchar(30) NOT NULL,
  `Detalle` varchar(400) NOT NULL,
  `TipoMaltrato` varchar(12) NOT NULL,
  `CantidadAgresores` int(2) NOT NULL,
  `FechaIncidente` date NOT NULL,
  `DireccionSuceso` varchar(200) NOT NULL,
  `Evidencia` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `denuncia`
--

INSERT INTO `denuncia` (`idDenuncia`, `docid`, `Nombre`, `Apellido`, `Celular`, `Correo`, `Detalle`, `TipoMaltrato`, `CantidadAgresores`, `FechaIncidente`, `DireccionSuceso`, `Evidencia`) VALUES
(1, 32644858, 'Nisa Belen', 'Agurto Huertas', 986393817, 'NissaAgurto@gmail.com', 'aaa', 'Sexual', 2, '2021-05-03', 'calle san pedro', NULL),
(2, 75452498, 'Sergio André', 'Berru Escajadillo', 955567066, 'Sergio_Berru@gmail.com	', 'bbb', 'Psicológico', 1, '2021-05-11', 'calle san jacinto', NULL),
(3, 82673898, 'Carlos Gabriel', 'Masias Ordinola', 967075106, 'Masias2021@gmail.com', 'ccc', 'Económico', 1, '2021-05-07', 'calle 12 jr vicus', NULL),
(4, 72556212, 'Pedro Martin', 'Mazanares Valle', 950956148, 'Pedo_Martin@gmail.com', 'ddd', 'Físico', 1, '2021-05-13', 'calle piura', NULL),
(5, 74652761, 'Claudia Isabel', 'Moscol Renteria', 975740690, 'Claudia_Moscol@gmail.com', 'eee', 'Sexual', 3, '2021-05-10', 'calle san juan 39', NULL),
(6, 71232761, 'Luis Fernando', 'Yarleque Golles', 991835173, 'Luis_Yarleque@gmail.com', 'eee', 'Económico', 3, '2021-04-20', 'calle san top 09', NULL),
(7, 77132032, 'Jorge', 'Ricardi Rivera', 985632369, 'jorge_ricardi@gmail.com', 'Agresión física', 'Físico', 2, '2021-05-15', 'Calle primavera 152', NULL),
(8, 77132032, 'Renzo', 'Olívares Ato', 985685698, 'renzooa@gmail.com', 'Agresión Sexual', 'Sexual', 1, '2021-05-25', 'Calle Matacaballo 153', NULL),
(9, 75534311, 'Roberto', 'Rodriguez', 995241752, 'rob_as@gmail.com', 'Ejempl odetalle xd', 'Sexual', 3, '0000-00-00', 'Avenida santa xDassa', NULL),
(15, 12345678, 'Don Ramon', 'Chavoooo', 12345678, 'ramon@gmail.com', 'asdsdaasd asdsadsadsadsad asdasdasdasd', '1', 2, '2021-06-07', 'Ramon don don xD', '1623097833_152378bddc9c8999a637.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `keys`
--

CREATE TABLE `keys` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `key` varchar(40) NOT NULL,
  `level` int(2) NOT NULL,
  `ignore_limits` tinyint(1) NOT NULL DEFAULT 0,
  `is_private_key` tinyint(1) NOT NULL DEFAULT 0,
  `ip_addresses` text DEFAULT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `keys`
--

INSERT INTO `keys` (`id`, `user_id`, `key`, `level`, `ignore_limits`, `is_private_key`, `ip_addresses`, `date_created`) VALUES
(1, 1, '123456', 0, 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `miembro`
--

CREATE TABLE `miembro` (
  `comiembro` int(10) UNSIGNED NOT NULL,
  `cotipom` int(10) UNSIGNED NOT NULL,
  `fechareg` date NOT NULL,
  `codper` int(10) NOT NULL,
  `idDenuncia` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `miembro`
--

INSERT INTO `miembro` (`comiembro`, `cotipom`, `fechareg`, `codper`, `idDenuncia`) VALUES
(1, 1, '2017-02-02', 21, 1);

-- --------------------------------------------------------

--
-- Table structure for table `paginas`
--

CREATE TABLE `paginas` (
  `idpagina` int(10) UNSIGNED NOT NULL,
  `controlador` varchar(250) NOT NULL,
  `metodo` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

--
-- Dumping data for table `paginas`
--

INSERT INTO `paginas` (`idpagina`, `controlador`, `metodo`) VALUES
(1, 'privado', 'index'),
(2, 'privado', 'doSave'),
(3, 'privado', 'doList'),
(4, 'home', 'index');

-- --------------------------------------------------------

--
-- Table structure for table `tipomiembro`
--

CREATE TABLE `tipomiembro` (
  `cotipom` int(10) UNSIGNED NOT NULL,
  `descripcion` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipomiembro`
--

INSERT INTO `tipomiembro` (`cotipom`, `descripcion`) VALUES
(1, 'Jefe de Familia'),
(2, 'Familiar Directo'),
(3, 'Victima');

-- --------------------------------------------------------

--
-- Table structure for table `tipo_usuario`
--

CREATE TABLE `tipo_usuario` (
  `id_tipo` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tipo_usuario`
--

INSERT INTO `tipo_usuario` (`id_tipo`, `descripcion`) VALUES
(4, 'Administrador'),
(5, 'Moderador'),
(6, 'Visita');

-- --------------------------------------------------------

--
-- Table structure for table `usuario`
--

CREATE TABLE `usuario` (
  `id_user` int(11) NOT NULL,
  `login` char(20) NOT NULL,
  `clave` text NOT NULL,
  `id_tipo` int(11) NOT NULL,
  `descripcion` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `usuario`
--

INSERT INTO `usuario` (`id_user`, `login`, `clave`, `id_tipo`, `descripcion`) VALUES
(5, 'fernando@gmail.com', '$2y$10$1uMxVboNrzjI4wtU8Ykqxeu8p5i9gsms6wmQ.12mFGub5g6FUpJLW', 4, 'ADMIN '),
(6, 'sergio@gmail.com', '$2y$10$1uMxVboNrzjI4wtU8Ykqxeu8p5i9gsms6wmQ.12mFGub5g6FUpJLW', 4, 'Usuario de prueba');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `denuncia`
--
ALTER TABLE `denuncia`
  ADD PRIMARY KEY (`idDenuncia`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `denuncia`
--
ALTER TABLE `denuncia`
  MODIFY `idDenuncia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
